﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleMovement : MonoBehaviour {

    //capsule speed when it's moving
    public float speed = 1f;

	// Use this for initialization
	void Start () {
        //move one unity space unit per second towards front direction  (only once at initialization)
        transform.Translate(new Vector3(speed * Time.deltaTime,0,0));
        Debug.Log("Moving From Start Event");
	}
	
	// Update is called once per frame
	void Update () {
        //move one unity space unit per second towards right direction (executed every frame)
        transform.Translate(new Vector3(0, 0, -speed * Time.deltaTime));
        Debug.Log("Moving From Update Event");
    }
}
