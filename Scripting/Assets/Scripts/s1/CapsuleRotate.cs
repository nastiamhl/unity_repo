﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleRotate : MonoBehaviour {

    // degrees to rotate
    public float rotationDegrees = 1f;

	// Use this for initialization
	void Start () {
        // rotate rotationDegrees per second along y axis(only first frame)
        transform.Rotate(new Vector3(0, rotationDegrees* Time.deltaTime));
        Debug.Log("Rotating from Start Event");
	}
	
    
	// Update is called once per frame
	void Update () {
        // rotate rotationDegrees per second along y axis(executed every frame)
        transform.Rotate(new Vector3(0, rotationDegrees * Time.deltaTime));
        Debug.Log("Rotating from Update Event");
    }
}
