﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleScale : MonoBehaviour {

    // capsule scaleUnits when it's moving
    public float scaleUnits = 1f;
    // Use this for initialization
    void Start () {
        // add scaleUnits per second to transform localscale x axis (only once)
        transform.localScale = new Vector3(transform.localScale.x * scaleUnits, 1,1);
        Debug.Log("Scaling from Start Event");
	}
	
	// Update is called once per frame
	void Update () {
        // add scaleUnits per second to transform localscale x axis (only once)
        transform.localScale = new Vector3(transform.localScale.x * scaleUnits, 1, 1);
        Debug.Log("Scaling from Update Event");
    }
}
