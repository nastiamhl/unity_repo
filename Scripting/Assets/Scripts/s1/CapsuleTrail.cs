﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleTrail : MonoBehaviour {

    private TrailRenderer trail;

    public Texture NewTexture; // Drag texture into slot from Inspector

  
    // with Range it is possible to limit the values that variables can take
    [Range(1, 50)]
    public float startWidth = 3;

    [Range(1, 50)]
    public float endWidth = 2;

    private void Start()
    {
        // getting component's reference present in the same gameObject
        if (!(trail = GetComponent<TrailRenderer>()))
        {
           
            Debug.LogError("No TrailRenderer Component attached to the GameObject ");
            enabled = true;
            return;
        }
        GetComponent<TrailRenderer>().material.mainTexture = NewTexture;

    }


    // Update is called once per frame
    void Update () {
        //accessing trailRenderer present in the same gameObject
        trail.time = Random.Range(1, 100);
        trail.startWidth = startWidth;
        trail.endWidth = endWidth;
    }
}
