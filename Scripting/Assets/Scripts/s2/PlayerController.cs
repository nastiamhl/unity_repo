﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    // Use this for initialization
    public WaypointController waypointControl;
    public float speed = 5;
    private GameObject cam;

    private void Start()
    {
        cam = GameObject.Find("Main Camera");
    }

    // Update is called once per frame
    void Update () {
		float step = speed * Time.deltaTime;
        Vector3 currentWaypointPos = waypointControl.getCurrentWaypointPos();


        transform.position = Vector3.MoveTowards(transform.position, currentWaypointPos, step);

        cam.transform.position = transform.position - Vector3.forward * 10f;

	}
}
