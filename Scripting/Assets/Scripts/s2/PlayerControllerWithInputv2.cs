﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerWithInputv2 : MonoBehaviour {

    public float speed;
    public float runSpeed;
    private float hInput, vInput;
    public float rotationDegrees = 1f;
    public bool enabled_x;

    private void Start(){

        if (enabled_x)
        {
            
            if (hInput != 0)
            {
                transform.Rotate(new Vector3(0, rotationDegrees * Time.deltaTime));
                Debug.Log("Rotating from Start Event");
            }

        }

    }

    // Update is called once per frame
    void Update () {


        hInput = Input.GetAxisRaw("Horizontal");
        vInput = Input.GetAxisRaw("Vertical");

       // float step = speed;

        //if (Input.GetButton("Run"))
        //{
       //     step = runSpeed;
       // }

        //if (vInput != 0)
       // {
       //     transform.Translate(vInput * Vector3.forward * Time.deltaTime * step);
       // }

        if (enabled_x)
        {
            
            if (hInput != 0)
            {
                transform.Rotate(new Vector3(0, rotationDegrees * Time.deltaTime));
                Debug.Log("Rotating from Update Event");
            }
            
        }
  
    }
}
