﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerWithInputv3 : MonoBehaviour {

    public Color clr; //visible in the Unity Editor Color variable 
    private Color start_clr; // primary color of the object

    public bool pressed = false; // a variable that will show whether the key is pressed or not
    Material mat; // the material that will be changed
  
	// Use this for initialization
	void Start () {

        mat = GetComponent<Renderer>().material; // getting the material from object
        start_clr = mat.color;// getting primary color into the variable start_clr
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            pressed = true;
            mat.color = clr;// getting color from users input
        }

        else if (Input.GetKeyUp(KeyCode.Space)) {
            pressed = false;
            mat.color = start_clr; // clearing color to primary when the key is released
        }
           
    }
}
