﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerWithInputv4 : MonoBehaviour {

    // capsule scaleUnits when it's moving

    public Vector3 start_scale;
    public float scaleUnits_x = 3f; // fixed scale Units to be applied
    public float scaleUnits_y = 2f;
    public float scaleUnits_z = 3f;

    // Use this for initialization
    void Start()
    {
        start_scale = transform.localScale; // getting current scale
    }

    // Update is called once per frame
    void Update()
    {
        //6.1 - fixed scale change 

        //if (Input.GetKeyDown(KeyCode.G)) transform.localScale = new Vector3(transform.localScale.x * scaleUnits_x, transform.localScale.y * scaleUnits_y, transform.localScale.z * scaleUnits_z);
        //else if (Input.GetKeyUp(KeyCode.G)) transform.localScale = start_scale; // returning to primary scale

        //6.2 - random scale change
        if (Input.GetKeyDown(KeyCode.G)) transform.localScale = new Vector3(transform.localScale.x * Random.Range(0, 5f), transform.localScale.y * Random.Range(0, 5f), transform.localScale.z * Random.Range(0, 5f));
        else if (Input.GetKeyUp(KeyCode.G)) transform.localScale = start_scale; // returning to primary scale


    }
}
