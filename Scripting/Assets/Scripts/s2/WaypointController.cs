﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointController : MonoBehaviour {

    private GameObject[] waypoints;
    private int currentWayPointIndex = 0;
    public PlayerController player;

	// Use this for initialization
	void Start () {
        waypoints = GameObject.FindGameObjectsWithTag("Waypoint");
        if (waypoints == null){
            Debug.Log("No waypoints found in the scene");
        }

        player = GameObject.FindWithTag("PlayerController").GetComponent<PlayerController>();
        if (!player)
        {
            Debug.Log("No GameObject found with Player Tag");
            enabled = false;
            return;
        }
	}
	
	// Update is called once per frame
	void Update () {
		if (player.transform.position.Equals(waypoints[currentWayPointIndex].transform.position)) {
            currentWayPointIndex++;

            if (currentWayPointIndex >= waypoints.Length)
                currentWayPointIndex = 0;
        }
	}
    public Vector3 getCurrentWaypointPos()
    {
        return waypoints[currentWayPointIndex].transform.position;
    }
}
