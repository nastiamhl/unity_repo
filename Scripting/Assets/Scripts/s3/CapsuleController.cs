﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CapsuleController : MonoBehaviour
{
    //Camera Movement
    public float lookSensitivity=1f; // sensitivity of movement

    //Movement
    public float speed;//speed of movement
    public float jump;//power of jump

    public Camera cam;// camera object

    private Rigidbody body;//rigidbody object
  

    void Start () {
        body = GetComponent<Rigidbody>();// storing rigidbody of the capsule in variable called "body" 
    }
	
	// Update is called once per frame
	void Update () {

        //changing property velocity of RigidBody depending on a reason

        //jumping
        if (Input.GetKeyDown(KeyCode.Space))
            body.velocity = new Vector3(0, 10, 0);

        // moving forward
        if (Input.GetKeyDown(KeyCode.UpArrow))
            body.velocity = new Vector3(0, 0, 5);

        //moving backward
        if (Input.GetKeyDown(KeyCode.DownArrow))
            body.velocity = new Vector3(0, 0, -5);
        //moving left
        if (Input.GetKeyDown(KeyCode.LeftArrow))
            body.velocity = new Vector3(-5, 0, 0);
        //moving right
        if (Input.GetKeyDown(KeyCode.RightArrow))
            body.velocity = new Vector3(5, 0, 0);


        // capsule movement left/right with a mouse
        float around_y = Input.GetAxisRaw("Mouse X");
        Vector3 rotate_y = new Vector3(0, around_y, 0) * lookSensitivity;
        transform.Rotate(rotate_y);

        // camera(attached to capsule) movement upwards downwards with a mouse
        float around_x = Input.GetAxisRaw("Mouse Y");
        Vector3 rotate_x = new Vector3(around_x, 0, 0) * lookSensitivity;
        cam = gameObject.GetComponent<Camera>();
        cam.transform.Rotate(-rotate_x);


    }

    
}
