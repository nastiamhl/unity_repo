﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCapsule : MonoBehaviour {
    // a script that will destroy an object when it will collide with a sphere
    private void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Sphere(Clone)" || col.gameObject.name == "Sphere") // if a object with which capsule has made a collision is a sphere or its clone
        {
            Destroy(gameObject); // destroy the capsule
            Debug.Log("Capsule is destroyed as a result of collision with sphere object"); // and send the message to Unity console;
        }
    }


}
