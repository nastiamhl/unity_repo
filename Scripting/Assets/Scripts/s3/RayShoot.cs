﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayShoot : MonoBehaviour {

    public float range = 100f;//a range from which you can shoot and raycast will detect it

    public Camera fpsCam;// camera object


	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Mouse0))// if the left button of the mouse is pressed
        {
            Shoot();//realise function Shoot()
        }
	}

    void Shoot()
    {
        RaycastHit hit; //storing information about the hit

        if(Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))// if a raycast detect a certain object towards him within a certain range
        {
           Debug.Log(hit.transform.name);//writing to console name of the object that has been hit
           Target target =  hit.transform.GetComponent<Target>();// getting to the script target of the capsule
           if( target != null)//if target is not null (which will be true only if we aim on a sphere or its clone)
            {
                target.Die();// go to method Die() in target script, which means destroy sphere
            }

        }
    }
}
