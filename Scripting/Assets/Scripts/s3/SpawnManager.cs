﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// a script that will manage work of spawnpoints and will randomly choose one of them to instantiate an object every period of time set 
public class SpawnManager : MonoBehaviour {

    public GameObject object_spawn; // object to be instantiated
    public float instant_freq;// frequency with which manager will generate objects

    
    public Color clr; // a color of the spawned object, visible in the Unity Editor
    Material mat;// a material which later will be assigned
    private Color start_clr;//will be primary color of an object
    public float obj_next = 0;//a counter that will count in realtime how much clones have been generated during the game



    [Range(1,70)]
    public float forcePower; // a variable that will measure the power of force which will be randomly picked
    private Rigidbody rb;// rigidbody of a sphere to apply forces to it later

    [Range(1,10)]
    public float object_scale; // an object scale rang which will be randomly picked
    public Transform[] spawnPoints; // an array that will store all four spawnPoints in it

    // Use this for initialization
    void Start () {

        InvokeRepeating("Spawn", instant_freq, instant_freq); // a functon that wll repeat "Spawn" function every instant_freq seconds

    }
    
    void Spawn()
    {

        
        int spawnPointIdex = Random.Range(0, spawnPoints.Length); //generating a random index of a spawnpoint stored in an array 

        
        object_scale = Random.Range(1f, 10f); // random scale of an object
        object_spawn.transform.localScale = new Vector3(transform.localScale.x * object_scale, transform.localScale.y * object_scale, transform.localScale.z * object_scale); // applying random scale to a sphere that will be spawned

        GameObject made = Instantiate(object_spawn, spawnPoints[spawnPointIdex].position, spawnPoints[spawnPointIdex].rotation);// instantiating sphere modified earlier in a spawnpoint randomly picked from an array before
        Debug.Log("A sphere clone was generated from the spawpoint " + (spawnPointIdex+1)); // sending a message to the Unity console
        obj_next++;// increasing counter of spawned spheres on 1 point
        made.GetComponent<Renderer>().material.color = clr;// changing the color of the instantiated

        rb = made.GetComponent<Rigidbody>(); //storing in rb a rigibody of the sphere
        forcePower = Random.Range(1f,70f);// generating a random force power
        rb.AddForce(Random.onUnitSphere * forcePower);//applying a random force and power to the sphere


    }
}
