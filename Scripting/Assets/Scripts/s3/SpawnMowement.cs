﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMowement : MonoBehaviour {

    public GameObject center;
    public float speed;
    public float radius;
    Vector3 radius_rotation; 

    // Use this for initialization
    void Start () {
        //radius_rotation = new Vector3(center.transform.position.x - radius, center.transform.position.y, center.transform.position.z + radius);
        transform.position = new Vector3(center.transform.position.x + radius, center.transform.position.y, center.transform.position.z + radius);
    }

    

    // Update is called once per frame
    void Update () {
    }

    private void FixedUpdate()
    {
        OrbitAround();
    }

    void OrbitAround()
    {
        transform.RotateAround(center.transform.position, Vector3.up, speed * Time.deltaTime);
    }
}

