﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour {
    // a scipt that is responsible for destroying a target of shooting
    public void Die()
    {
        Destroy(gameObject);
    }
}
